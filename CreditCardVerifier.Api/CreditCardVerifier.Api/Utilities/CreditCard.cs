﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using TalantonHR.Enum;

namespace TalantonHR.Core
{
    public class CreditCard
    {
       
        // Class to hold credit card type information
        public class CreditCardInfo
        {
            public CreditCardInfo(string regEx, int length, CardTypes type)
            {
                RegEx = regEx;
                Length = length;
                Type = type;
            }

            public string RegEx { get; set; }
            public int Length { get; set; }
            public CardTypes Type { get; set; }
        }

        // Array of CardTypeInfo objects.
        // Used by GetCardType() to identify credit card types.
        public static CreditCardInfo[] _creditCardInfo =
        {
            new CreditCardInfo("^(51|52|53|54|55)", 16, CardTypes.Master),
            new CreditCardInfo("^(4)", 16, CardTypes.Visa),
            new CreditCardInfo("^(34|37)", 15, CardTypes.Amex),
            new CreditCardInfo("^(3)", 16, CardTypes.JCB),
        };

        public CardTypes GetCardType(string cardNumber)
        {
            foreach (CreditCardInfo info in _creditCardInfo)
            {
                if (cardNumber.Length == info.Length &&
                    Regex.IsMatch(cardNumber, info.RegEx))
                    return info.Type;
            }

            return CardTypes.Unknown;
        }

        public bool ValidateYear(int year, string cardType)
        {
            bool result = true;
            if (DateTime.Now.Year > year)
            {
                result = false;
            }
            else
            {
                if (cardType == CardTypes.Visa.ToString())
                {
                    result = DateTime.IsLeapYear(year);
                }
                else if (cardType == CardTypes.Master.ToString())
                {
                    int i, ctr = 0;
                    for (i = 2; i <= year / 2; i++)
                    {
                        if (year % i == 0)
                        {
                            ctr++;
                            break;
                        }
                    }
                    if (ctr == 0 && year != 1)
                        result = true;
                    else
                        result = false;
                    } 
                }
            return result;
            }
      }
}
