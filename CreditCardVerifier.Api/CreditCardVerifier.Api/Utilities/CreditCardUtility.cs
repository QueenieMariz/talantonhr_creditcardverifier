﻿using System;
using System.Reflection.Metadata;

namespace TalantonHR.Core
{
    public class CreditCardUtility
    {
        protected static CreditCardUtility _instance = new CreditCardUtility();
        protected CreditCard _card;
        public CreditCard Card
        {
            get
            {
                if (this._card == null)
                    this._card = new CreditCard();
                return this._card;
            }
            set
            {
                this._card = value;
            }
        }
        public static CreditCardUtility Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new CreditCardUtility();
                return _instance;
            }
            set
            {
                _instance = value;
            }
        }

        private CreditCardUtility()
        {
        }

        static CreditCardUtility()
        {
        }

        public static String GetTypeName(String cardNumber)
        {
            return  Instance.Card.GetCardType(cardNumber.Replace(" ", string.Empty)).ToString();
        }
        public static bool ValidateYear(int year, string cardType)
        {
            return Instance.Card.ValidateYear(year, cardType);
        }
    }
}
