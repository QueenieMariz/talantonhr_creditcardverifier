﻿using System;
namespace TalantonHR.Enum
{
    public enum CardTypes
    {
        Amex = 0,
        JCB = 1,
        Master = 2,
        Visa = 3,
        Unknown = 4
    }
}
