﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditCardVerifier.Api.Models
{

    public class CreditCardData
    {
        public int Id { get; set; }
        public long CardNum { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string CardType { get; set; }
        public string Validity { get; set; }
    }
}
