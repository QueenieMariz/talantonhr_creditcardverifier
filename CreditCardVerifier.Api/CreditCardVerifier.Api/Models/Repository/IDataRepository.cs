﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditCardVerifier.Api.Models.Repository
{
    public interface IDataRepository
    {
        IEnumerable<CreditCard> GetAll();
        object Get(long cardNumber);
        CreditCard GetCard(int id);
        long Add(CreditCard b);
        int Delete(int id);
    }
}
