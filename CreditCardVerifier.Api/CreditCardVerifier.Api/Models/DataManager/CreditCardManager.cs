﻿using CreditCardVerifier.Api.Context;
using CreditCardVerifier.Api.Models.Repository;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using TalantonHR.Core;
using TalantonHR.Enum;

namespace CreditCardVerifier.Api.Models.DataManager
{
    public class CreditCardManager: IDataRepository
    {
        MainContext _context = new MainContext();

        public long Add(CreditCard creditCard)
        {
            _context.CreditCards.Add(creditCard);
            long cardNum = _context.SaveChanges();
            return cardNum;
        }

        public int Delete(int id)
        {
            int cardId = 0;
            var creditCard = _context.CreditCards.FirstOrDefault(b => b.Id == id);
            if (creditCard != null)
            {
                _context.CreditCards.Remove(creditCard);
                cardId = _context.SaveChanges();
            }
            else
            {

            }
            return cardId;
        }

        public CreditCard GetCard(int id)
        {
            return _context.CreditCards.Find(id);
        }

        public object Get(long cardNum)
        {
            // var creditCard = _context.CreditCards.FirstOrDefault(b => b.CardNum == cardNum);
            var creditCard = _context.CreditCards
               .FromSql("sp_CheckIfExist @CardNum = {0}", cardNum)
               .AsNoTracking().ToList();

            CreditCardData data = new CreditCardData();
            if (creditCard.Count == 0)
            {
                data.Validity = "Does not exist";
                return data;
            }
           
            String cardType = CreditCardUtility.GetTypeName(Convert.ToString(cardNum));
            bool validYear = CreditCardUtility.ValidateYear(creditCard[0].Year, cardType);

            if (cardType != CardTypes.Unknown.ToString() && validYear)
            {
                data.CardType = cardType;
                data.Validity = "Valid";
            }
            else if (cardType != CardTypes.Unknown.ToString() && !validYear)
            {
                data.CardType = cardType;
                data.Validity = "Invalid";
            }
            else
            {
                data.CardType = cardType;
                data.Validity = "Invalid";
            }
            return data;
        }

        public IEnumerable<CreditCard> GetAll()
        {
            var creditCard = _context.CreditCards.ToList();
            return creditCard;
        }
    }
}
