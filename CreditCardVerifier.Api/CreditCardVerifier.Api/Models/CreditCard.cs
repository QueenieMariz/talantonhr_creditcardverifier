﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CreditCardVerifier.Api.Models
{
    public class CreditCard
    {
        [Key]
        public int Id { get; set; }
        public long CardNum { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
}
