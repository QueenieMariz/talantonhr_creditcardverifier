﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditCardVerifier.Api.Models;
using CreditCardVerifier.Api.Models.Repository;
using Microsoft.AspNetCore.Mvc;

namespace CreditCardVerifier.Api.Controllers
{
    [Route("api/[controller]")]
    public class CreditCardController : Controller
    {
        private IDataRepository _iRepo;

        public CreditCardController(IDataRepository repo)
        {
            _iRepo = repo;
        }

        // GET: api/<controller>
        [HttpGet()]
        public IEnumerable<CreditCard> Get()
        {
            return _iRepo.GetAll();
        }

        // GET api/<controller>/5
        [HttpGet("{cardNum}", Name = "GetCC")]
        public object GetCard(long cardNum)
        {
            try
            {
                if (false) return BadRequest();
                return Ok(_iRepo.Get(cardNum));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST api/<controller>
        [HttpPost()]
        public IActionResult Create([FromBody]CreditCard creditCard)
        {
            try
            {
                if (creditCard == null)
                {
                    return BadRequest();
                }
                return Ok(_iRepo.Add(creditCard));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _iRepo.Delete(id);
        }
    }
}
