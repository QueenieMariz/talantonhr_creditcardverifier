using CreditCardVerifier.Api.Context;
using CreditCardVerifier.Api.Controllers;
using CreditCardVerifier.Api.Models;
using CreditCardVerifier.Api.Models.Repository;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CreditCardApi.Test
{
    [TestClass]
    public class UnitTest1
    {
        private Mock<IDataRepository> _mockDataRepo;
        private CreditCardController controller;

        [TestInitialize]
        public void Init()
        {
            _mockDataRepo = new Mock<IDataRepository>();
            controller = new CreditCardController(_mockDataRepo.Object);
        }

        [TestMethod]
        public void GetCardNum()
        {
            //Act
            var ccRes = controller.GetCard(3530111333300022);

            //Assert
            ccRes.Should().BeOfType<OkObjectResult>().Which.StatusCode.Should().Equals(200);
            ccRes.Should().BeOfType<OkObjectResult>().Which.Value.Should().Equals(ccRes);
        }

    }
}
