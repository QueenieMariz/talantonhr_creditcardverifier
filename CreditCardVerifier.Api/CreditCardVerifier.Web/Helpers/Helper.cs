﻿using RestSharp;
using System.ComponentModel.DataAnnotations;

namespace CreditCardVerifier.Web.Helpers
{
    public class CreditCardApi
    {
        private string _apiBaseURI = "http://localhost:54365";

        public RestClient InitializeClient()
        {
            var client = new RestClient(_apiBaseURI);
            client.AddDefaultHeader("Content-type", "application/json");
            return client;
        }
    }
    
    public class CreditCardData
    {
        public int? Id { get; set; }
        [Required]
        [Display(Name = "Card number")]
        public long? CardNum { get; set; }
        [Required]
        [Range(1,12, ErrorMessage ="Invalid month")]
        public int? Month { get; set; }
        [Required]
        public int? Year { get; set; }
        [Display(Name = "Card type")]
        public string CardType { get; set; }
        public string Validity { get; set; }
    }
}
