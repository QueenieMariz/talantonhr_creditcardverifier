﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditCardVerifier.Web.Helpers;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace CreditCardVerifier.Web.Controllers
{
    public class CreditCardsController : Controller
    {
        CreditCardApi _creditCardApi = new CreditCardApi();

        public async Task<IActionResult> Index()
        {
            List<CreditCardData> dto = new List<CreditCardData>();

            RestClient client = _creditCardApi.InitializeClient();
            var request = new RestRequest("/api/creditcard/", Method.GET);
            IRestResponse _response = client.Execute(request);

            if (_response.IsSuccessful)
            {
                var result = _response.Content;
                dto = JsonConvert.DeserializeObject<List<CreditCardData>>(result);
            }

            return View(dto);
        }

        public IActionResult Validate()
        {
                ViewBag.Validated = false;
                return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Validate([Bind("CardNum")] long cardNum)
        {
            CreditCardData dto = new CreditCardData();

            RestClient client = _creditCardApi.InitializeClient();
            var request = new RestRequest("/api/creditcard/" + cardNum, Method.GET);
            IRestResponse _response = client.Execute(request);

            if (_response.IsSuccessful)
            {
                var result = _response.Content;
                dto = JsonConvert.DeserializeObject<CreditCardData>(result);
            }
            ViewBag.Validated = true;
            return View(dto);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id, CardNum, Month, Year")] CreditCardData creditCardData)
        {
            if (ModelState.IsValid)
            {
                RestClient client = _creditCardApi.InitializeClient();
                var request = new RestRequest("/api/creditcard/", Method.POST);
                request.AddJsonBody(
                    new
                    {
                        CardNum = creditCardData.CardNum,
                        Month = creditCardData.Month,
                        Year = creditCardData.Year
                    });

                IRestResponse _response = client.Execute(request);
                if (_response.IsSuccessful)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(creditCardData);
        }


        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            List<CreditCardData> dto = new List<CreditCardData>();
            RestClient client = _creditCardApi.InitializeClient();
            var request = new RestRequest("/api/creditcard/", Method.GET);
            IRestResponse _response = client.Execute(request);

            if (_response.IsSuccessful)
            {
                var result = _response.Content;
                dto = JsonConvert.DeserializeObject<List<CreditCardData>>(result);
            }

            var creditCard = dto.SingleOrDefault(m => m.Id == id);
            if (creditCard == null)
            {
                return NotFound();
            }

            return View(creditCard);
        }


        [HttpPost, ActionName("Put")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(long id, [Bind("Id, CardNum, Month, Year")] CreditCardData creditCardData)
        {
            if (id != creditCardData.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                RestClient client = _creditCardApi.InitializeClient();
                var request = new RestRequest("/api/creditcard/" + id, Method.PUT);
                request.AddJsonBody(
                    new
                    {
                        CardNum = creditCardData.CardNum,
                        Month = creditCardData.Month,
                        Year = creditCardData.Year
                    });

                IRestResponse _response = client.Execute(request);
                if (_response.IsSuccessful)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(creditCardData);
        }


        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            List<CreditCardData> dto = new List<CreditCardData>();
            RestClient client = _creditCardApi.InitializeClient();
            var request = new RestRequest("/api/creditcard/", Method.GET);
            IRestResponse _response = client.Execute(request);

            if (_response.IsSuccessful)
            {
                var result = _response.Content;
                dto = JsonConvert.DeserializeObject<List<CreditCardData>>(result);
            }

            var creditCard = dto.SingleOrDefault(m => m.Id == id);
            if (creditCard == null)
            {
                return NotFound();
            }

            return View(creditCard);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(long id)
        {
            RestClient client = _creditCardApi.InitializeClient();
            var request = new RestRequest("/api/creditcard/" + id, Method.DELETE);
            IRestResponse _response = client.Execute(request);
            if (_response.IsSuccessful)
            {
                return RedirectToAction("Index");
            }

            return NotFound();
        }
    }
}